package com.dipesh.likeornot.home.data

import com.dipesh.core.extensions.toDataResult
import com.dipesh.core.networking.DataResult
import com.dipesh.likeornot.data.user.local.dao.ProfileDao
import com.dipesh.likeornot.data.user.local.entities.ProfileL
import com.dipesh.likeornot.home.interfaces.FetchProfile

class ProfileLocal(private val dao: ProfileDao) : FetchProfile.Local {

    override suspend fun getProfileList(): DataResult<List<ProfileL>> {
        return dao.getProfiles().toDataResult()
    }

    override suspend fun addProfiles(list: List<ProfileL>): List<ProfileL> {
        dao.insert(list)
        return dao.getProfiles()
    }

    override suspend fun updateStatus(profile: ProfileL) {
        dao.update(profile)
    }
}