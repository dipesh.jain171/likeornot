package com.dipesh.likeornot.home.interfaces

import com.google.gson.JsonObject
import com.dipesh.core.networking.DataResult
import com.dipesh.core.networking.callApi
import com.dipesh.likeornot.data.ApiService


class FetchProfilesRemote(
    private val apiService: ApiService

) : FetchProfile.Remote {

    override suspend fun getProfileList(): DataResult<JsonObject> =
        callApi { apiService.getProfile(10) }

}
