package com.dipesh.likeornot.home.interfaces

import com.google.gson.JsonObject
import com.dipesh.core.networking.DataResult
import com.dipesh.likeornot.data.user.local.entities.ProfileL


interface FetchProfile {

    interface Repository {
        suspend fun getProfileList(): DataResult<List<ProfileL>>
        suspend fun updateStatus(profile: ProfileL)
    }

    interface Local {
        suspend fun getProfileList(): DataResult<List<ProfileL>>
        suspend fun addProfiles(list: List<ProfileL>): List<ProfileL>
        suspend fun updateStatus(profile: ProfileL)
    }

    interface Remote {
        suspend fun getProfileList(): DataResult<JsonObject>
    }
}
