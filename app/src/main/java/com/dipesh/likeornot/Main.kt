package com.dipesh.likeornot

import com.dipesh.core.application.CoreApp
import com.dipesh.likeornot.data.AppDb
import com.dipesh.likeornot.data.util.daoModule
import com.dipesh.likeornot.data.util.webServiceModule


/**
 * Android's Application class.
 * Use for 3rd party library init and other setup.
 */
class Main : CoreApp() {

    override fun getDataModules() = arrayOf(
        daoModule(AppDb.getDatabase(applicationContext)),
        webServiceModule
    )

}
