package com.dipesh.likeornot.ui

import com.dipesh.core.ui.BaseActivity
import com.dipesh.likeornot.R

class HomeActivity : BaseActivity(R.layout.activity_home) {
    override val toolbarTitle: String
        get() = "LikeOrNot"
}