package com.dipesh.likeornot.data.util

import com.dipesh.likeornot.data.AppDb
import org.koin.dsl.module

fun daoModule(appDb: AppDb) = module {

    single { appDb.profileDao() }


}

