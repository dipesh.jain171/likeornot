package com.dipesh.likeornot.data.user.remote

import com.google.gson.annotations.SerializedName
import com.dipesh.likeornot.data.user.local.entities.ProfileL
import com.dipesh.likeornot.data.user.local.models.Info

data class ProfileR(
    @SerializedName("info") val info: Info,
    @SerializedName("results") val list: List<ProfileL>
)
