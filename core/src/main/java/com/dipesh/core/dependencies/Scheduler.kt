package com.dipesh.core.dependencies

import com.dipesh.core.dispatchers.AppDispatcher
import com.dipesh.core.dispatchers.Dispatcher
import org.koin.dsl.module

val schedulerModule = module {

    /** The application-level dispatcher for scheduling work on threads **/
    single<Dispatcher> { AppDispatcher() }

}
