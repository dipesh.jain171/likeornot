package com.dipesh.core.utils

private const val EMPTY_STRING = ""

fun emptyString() = EMPTY_STRING
